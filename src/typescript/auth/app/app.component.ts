import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

declare var localStorage: any;
declare var window: any;

@Component({
  selector: 'app',
  template: require('./view')
})
export class AppComponent {

    name: string = ''
    password: string = ''
    conflict: boolean = false
    wrong: boolean = false
    currentForm: string = 'up'

    constructor(private service: AuthService) {}

    private handleForm(data: any) {
        localStorage.setItem('spellshot_token', data.token);
        window.location.href = '/';
    }

    handlerName() {
        this.conflict = false;
    }

    switchToSignIn() {
        this.currentForm = 'in'
    }

    switchToSignUp() {
        this.currentForm = 'up'
    }

    signIn() {
        this.service.signIn(this.name, this.password)
            .subscribe(this.handleForm, (err: any) => this.wrong = true);
    }

    signUp() {
        this.service.signUp(this.name, this.password)
            .subscribe(this.handleForm, (err: any) => this.conflict = true);
    }
}
