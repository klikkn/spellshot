import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { api } from '../../../settings';

@Injectable()
export class AuthService {
  constructor (private http: Http) {}

    signIn(name: string, password: string) {
       return this.http.post(`${api}/sign/in`, { name, password })
           .map((res:Response) => res.json());
    }

    signUp(name: string, password: string) {
       return this.http.post(`${api}/sign/up`, { name, password })
           .map((res:Response) => res.json());
    }
}
