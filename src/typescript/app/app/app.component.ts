import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';
import { SocketService } from './services/socket.service';

@Component({
  selector: 'app',
  template: require('./view')
})
export class AppComponent implements OnInit {

    players: any = [{
        owner: null,
        icon: {},
        score: 0
    }]

    position = 0

    user: any = {
        _id: '',
        icon: {},
        name: '',
        resists: {
            frost: 0,
            water: 0,
            electro: 0,
            fire: 0
        },
        score: 0
    }

    spells: any = [
        { type: 'frost', name: 'Frost', active: false, icon1: '../icons/b-spell-frost.svg', icon2: '../icons/spell-1.svg', color: 'linear-gradient(45deg, #80FFEE, #006DA5)', cost: 7 },
        { type: 'water', name: 'Water', active: false, icon1: '../icons/b-spell-water.svg', icon2: '../icons/spell-2.svg', color: 'linear-gradient(45deg, #3198F3, #3144E3)', cost: 6 },
        { type: 'electro', name: 'Electro', active: false, icon1: '../icons/b-spell-electro.svg', icon2: '../icons/spell-3.svg', color: 'linear-gradient(45deg, #FFEF6A, #B47417)', cost: 8 },
        { type: 'fire', name: 'Fire', active: false, icon1: '../icons/b-spell-fire.svg', icon2: '../icons/spell-4.svg', color: 'linear-gradient(45deg, #FF6A6A, #A40847)', cost: 5 }
    ]

    selectedSpell: any = {
        name: '',
        type: '',
        active: false,
        icon1: '',
        icon2: '',
        color: '',
        cost: 0
    }

    openedModalType: String = ''

    selectedPlayer: any = {
        name: '',
        icon: {
            c1: '',
            c2: '',
        },
        owner: ''
    }

    isBubblesActive: boolean = false
    bubbleTimeout: any = null
    bubbleColor: any = ''

    isShowUpdateRools: Boolean = false

    constructor(private userService: UserService, private socket: SocketService) {
        this.userService.get()
            .subscribe((data: any) => {
                this.user = data.user
                this.players = data.players
                this.socket.auth(data.user.sessions[0].token)
                this.getPosition()
            });
    }

    ngOnInit(): void {
        this.socket.io.on('user', (user: any) => {
            this.user = user

            if (user.owner != user.uuid) {
                const player = this.players.find((o: any) => o.uuid == user.owner)
                this.selectedPlayer = player
            }
        })

        this.socket.io.on('players', (players: any) => {
            this.players = players

            if (this.selectedPlayer.name != '') {
                const player = this.players.find((o: any) => o.uuid == this.selectedPlayer.uuid)
                this.selectedPlayer.owner = player.owner
            }

            this.getPosition()
        });

        this.socket.io.on('damage', (type: any) => {
            this.isBubblesActive = true

            const spell = this.spells.find((spell: any) => spell.type == type)
            this.bubbleColor = spell.color


            if (!this.bubbleTimeout) {
                this.bubbleTimeout = setTimeout(() => {
                    this.isBubblesActive = false
                    this.bubbleTimeout = null
                }, 1500)
            }
        });
    }

    getOwning() {
        return this.players.filter((player: any) => {
            return player.owner === this.user.uuid && player.uuid !== this.user.uuid;
        })
    }

    getPlayers() {
        return this.players.sort((a: any, b: any) => b.score - a.score)
    }

    signOut() {
        this.userService.signOut();
        window.location.href = '/';
    }

    attack(player: any) {
        if (this.selectedSpell.name == '') return

        this.socket.io.emit('cast', {
            uuid: this.selectedPlayer.uuid,
            spell: this.selectedSpell.type,
            token: this.user.sessions[0].token,
        })

        this.openModal('loading')

        setTimeout(() => {
            const selected = this.players.find((player: any) => player.uuid == this.selectedPlayer.uuid)
            if (this.user.uuid == selected.owner) { this.openModal('result-win') }
            else { this.openModal('result-loose') }
        }, 1000)
    }

    restoreAccount() {
        this.socket.io.emit('restore', {
            uuid: this.selectedPlayer.uuid,
            spell: this.selectedSpell.type,
            token: this.user.sessions[0].token
        })

        this.openModal('loading')

        setTimeout(() => {
            if (this.user.uuid == this.user.owner) { this.openModal('result-win') }
            else { this.openModal('result-loose') }
        }, 1000)
    }

    selectPlayerForAttack(player: any) {
        if (player.uuid !== this.user.uuid && player.uuid === player.owner) {
            this.selectedPlayer = player
            this.openModal('attack')
        }
    }

    selectSpell(spell: any) {
        this.spells = this.spells.map((spell: any) => {
            spell.active = false
            return spell
        })

        spell.active = true
        this.selectedSpell = spell
    }

    openModal(type: String) {
        if (type == 'restore') {
            this.selectedPlayer = this.players.find((player: any) => player.uuid == this.user.owner)

            this.openModal('attack')
        } else {
            this.openedModalType = type
        }
    }

    closeModal() {
        this.openedModalType = ''
        this.selectedSpell = { type:'', name: '', active: false, icon1: '', icon2: '', color: '', cost: 0 }
    }

    tryAgain() {
        this.closeModal()
        this.openModal('attack')
    }

    calcPosition() {
        return this.getPlayers().findIndex((player: any) => player.uuid === this.user.uuid);
    }

    getPosition() {
        this.position = this.calcPosition() + 1
    }

    getOwner(): string {
        return this.players.find((player: any) => player.uuid === this.user.owner);
    }
}
