import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { api } from '../../../settings';

declare var localStorage: any;

@Injectable()
export class UserService {
    constructor (private http: Http) {}

    private headers = new Headers({
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('spellshot_token')
    });

    get() {
        return this.http.get(`${api}/init`, { headers: this.headers })
            .map((res:Response) => res.json());
    }

    signOut() {
        localStorage.removeItem('spellshot_token');
    }
}
