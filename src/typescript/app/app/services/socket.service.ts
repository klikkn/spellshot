import * as socketIo from 'socket.io-client';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { socketApi } from '../../../settings';


declare var localStorage: any;

@Injectable()
export class SocketService {
    io: any;

    constructor () {
        this.initSocket();
    }

    private initSocket(): void {
        this.io = socketIo(socketApi);
    }

    public auth(user: any): void {
        this.io.emit('auth', user);
    }
}
