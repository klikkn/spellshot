import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';

export function bootstrap(user: any) {
    platformBrowserDynamic().bootstrapModule(AppModule);
}
