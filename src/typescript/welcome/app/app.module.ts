import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpModule, JsonpModule } from '@angular/http';
import { ResistsService } from './services/resists.service';
import { UserService } from './services/user.service';

@NgModule({
  imports: [ BrowserModule, FormsModule, HttpModule, JsonpModule ],
  declarations: [ AppComponent ],
  bootstrap: [ AppComponent ],
  providers: [ ResistsService, UserService ]
})
export class AppModule { }
