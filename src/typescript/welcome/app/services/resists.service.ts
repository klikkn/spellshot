import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { api } from '../../../settings';

@Injectable()
export class ResistsService {
    constructor (private http: Http) {}

    private headers = new Headers({
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('spellshot_token')
    });

    set(frost: number, electro: number, water: number, fire: number) {
        return this.http.put(`${api}/resists`, { frost, electro, water, fire }, { headers: this.headers })
            .map((res:Response) => res.json());
    }
}
