import { Component, OnInit } from '@angular/core';
import { ResistsService } from './services/resists.service';
import { UserService } from './services/user.service';

declare var window: any;

@Component({
  selector: 'app',
  template: require('./view')
})
export class AppComponent implements OnInit {
    points: number = 100

    frost: number = 0
    electro: number = 0
    water: number = 0
    fire: number = 0

    user: any = {}

    activeManualTab: number = 1

    constructor(private resistsService: ResistsService, private userService: UserService) {}

    ngOnInit(): void {
        this.userService.get()
            .subscribe((user: any) => this.user = user);
    }

    private handleSet(user: any) {
        window.location.href = '/';
    }

    isShowUpdateRools: Boolean = false

    // ++
    increase(type: string) {
        if (this.points == 0) {
            return false;
        }

        this.points = this.points - 5;

        switch (type) {
            case "frost":
                this.frost = this.frost + 5
                break;

            case "electro":
                this.electro = this.electro + 5
                break;

            case "water":
                this.water = this.water + 5
                break;

            case "fire":
                this.fire = this.fire + 5
                break;

            default:
                break;
        }
    }

    // --
    decrease(type: string) {
        if (this.points == 100) {
            return false;
        }

        this.points = this.points + 5;

        switch (type) {
            case "frost":
                this.frost = this.frost == 0 ? 0 : this.frost - 5
                break;

            case "electro":
                this.electro = this.electro == 0 ? 0 : this.electro - 5
                break;

            case "water":
                this.water = this.water == 0 ? 0 : this.water - 5
                break;

            case "fire":
                this.fire = this.fire == 0 ? 0 : this.fire - 5
                break;

            default:
                break;
        }
    }

    setResists() {
        if (this.points > 0) {
            return false;
        }

        this.resistsService.set(this.frost, this.electro, this.water, this.fire)
            .subscribe(this.handleSet, (err: any) => console.log(err));
    }

    setActiveManualTab(num: number) {
        this.activeManualTab = num
    }

    signOut() {
        this.userService.signOut();
        window.location.href = '/';
    }
}
